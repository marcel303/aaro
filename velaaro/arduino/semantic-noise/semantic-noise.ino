/*

Regarding timer settings,
From: https://playground.arduino.cc/Main/TimerPWMCheatsheet/

  Pins 9 and 10: controlled by timer 1 in phase-correct PWM mode (cycle length = 510)
    Setting   Divisor   Frequency
    0x01      1         31372.55
    0x02      8         3921.16
    0x03      64        490.20   <--DEFAULT
    0x04      256       122.55
    0x05      1024      30.64

    TCCR1B = (TCCR1B & 0b11111000) | <setting>;

  Pins 11 and 3: controlled by timer 2 in phase-correct PWM mode (cycle length = 510)
    Setting   Divisor   Frequency
    0x01      1         31372.55
    0x02      8         3921.16
    0x03      32        980.39
    0x04      64        490.20   <--DEFAULT
    0x05      128       245.10
    0x06      256       122.55
    0x07      1024      30.64

    TCCR2B = (TCCR2B & 0b11111000) | <setting>;


*/

static const int kNumMotors = 4;
static const int kRampIntervalInMilliseconds = 25;

static const int motor_pins[kNumMotors] = { 3, 11, 9, 10 };

enum Message
{
  kMessage_None = 0,
  kMessage_SetSpeed = 1,
  kMessage_ReportSpeed = 2
};

struct MotorData
{
  int pin = -1;

  unsigned long lastTick = 0;
  
  int8_t currentSpeed = 0;
  int8_t desiredSpeed = 0;
};

static MotorData motorData[kNumMotors];

static uint8_t read_state = 0xff;
static uint8_t read_value1 = 0;
static uint8_t read_value2 = 0;
static uint8_t read_value3 = 0;

static unsigned long lastLoopTick = 0;

static int32_t blinkPhase = 0;

static int32_t receiveTimeoutTimer = 0;

void setup()
{
  Serial.begin(9600);
  
  pinMode(LED_BUILTIN, OUTPUT);

  for (int i = 0; i < kNumMotors; ++i)
  {
    motorData[i].pin = motor_pins[i];
    pinMode(motor_pins[i], OUTPUT);
  }

  TCCR1B = (TCCR1B & B11111000) | B00000001; // set timer 1 divisor to 1 for PWM frequency of 31372.55 Hz
  TCCR2B = (TCCR2B & B11111000) | B00000001; // set timer 2 divisor to 1 for PWM frequency of 31372.55 Hz
}

void loop()
{
  //delay(10);
  
  while (Serial.available() > 0)
  {
    const uint8_t value = Serial.read();

    //Serial.print("rcv ");
    //Serial.println(value);

    receiveTimeoutTimer = 0;
    
    if (value == 0xff)
      read_state = 0;

    if (read_state != 0xff)
    {
      read_state++;
  
      if (read_state == 2)
        read_value1 = value;
      if (read_state == 3)
        read_value2 = value;
      if (read_state == 4)
        read_value3 = value;
    }

    if (read_state == 4)
    {
      read_state = 0xff;
      
      const uint8_t target = read_value1;
      const uint8_t message = read_value2;
      const uint8_t value = read_value3;

      if (target >= 0 && target < kNumMotors)
      {
        MotorData & m = motorData[target];

        if (message == kMessage_SetSpeed)
        {
          if (value >= 0 && value <= 100)
            m.desiredSpeed = value;
          else
            Serial.println("speed out of range");  
        }
      }
      else
      {
        Serial.println("target out of range");  
      }
    }
  }

  const unsigned long tick = millis();
  
  for (MotorData & m : motorData)
  {
    if (receiveTimeoutTimer > 2000)
    {
      m.desiredSpeed = 0;
    }
    
    if (tick < m.lastTick)
    {
      // millis wraps around every 50 days
      // reset lastTick when this happens
      
      Serial.println("resetting lastTick due to millis wrap around");
      
      m.lastTick = 0;
    }
    else
    {
      const unsigned long nextTick = m.lastTick + kRampIntervalInMilliseconds;

      if (tick >= nextTick)
      {
        m.lastTick = tick;
        
        if (m.currentSpeed < m.desiredSpeed)
          m.currentSpeed++;
        if (m.currentSpeed > m.desiredSpeed)
          m.currentSpeed--;

        // safe guard: the motor rotates too quickly above a duty cycle of 100. make sure we never get there
        
        if (m.currentSpeed > 100)
          m.currentSpeed = 0;

        // update the PWM duty cycle for this motor

        analogWrite(m.pin, m.currentSpeed);
      }
    }
  }

  if (lastLoopTick == 0)
    lastLoopTick = tick;

  if (tick > lastLoopTick)
  {
    const int32_t elapsedMillis = tick - lastLoopTick;

    receiveTimeoutTimer += elapsedMillis;

    if (elapsedMillis > 0 && elapsedMillis < 256)
    {
      blinkPhase += 256 * elapsedMillis * motorData[0].currentSpeed * 6 / 100;

      const int32_t phaseWrap = int32_t(256) * 1000;
      while (blinkPhase > phaseWrap)
        blinkPhase = blinkPhase - phaseWrap;
    }

    if (blinkPhase < 0)
    {
      Serial.println("resetting blinkPhase due to it being less than 0..");
      blinkPhase = 0;
    }
    
    digitalWrite(LED_BUILTIN, blinkPhase < int32_t(128) * 1000 ? LOW : HIGH);

#if 0
    Serial.print("phase ");
    Serial.print(blinkPhase);
    Serial.print(" speed ");
    Serial.print(motorData[0].currentSpeed);
    Serial.print(" millis ");
    Serial.print(elapsedMillis);
    Serial.println();
#endif

    for (int i = 0; i < kNumMotors; ++i)
    {
      const uint8_t bytes[4] =
      {
        0xff,
        kMessage_ReportSpeed,
        i,
        motorData[i].currentSpeed
      };

      Serial.write(bytes, 4);
    }
  }

  lastLoopTick = tick;
}

