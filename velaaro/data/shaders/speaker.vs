include engine/ShaderSkinnedVS.txt
include engine/ShaderUtil.txt

uniform float lightAmount;

shader_out vec4 v_color;

void main()
{
	vec4 position = unpackPosition();
	
	position = objectToProjection(position);
	
	vec3 normal = unpackNormal().xyz;
	vec3 normal_view = normalize(objectToView3(normal));

	vec3 light_dir = normalize(vec3(1, -3, 1));
	float light_intensity = max(0.0, -dot(normal_view, light_dir));
	light_intensity *= lightAmount;

	vec3 color = vec3(0.01, 0.01, 0.01);
	color += vec3(0.8, 0.9, 1.0) * light_intensity;
	
	color = linearToSrgb(color);
	
	gl_Position = position;
	
	v_color = vec4(color, 1.0);
}
