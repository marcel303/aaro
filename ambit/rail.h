#pragma once

#include "Mat4x4.h"

struct Rail
{
	Mat4x4 objectToWorld;

	Vec3 position;

	void init();
	
	void tick(const float dt);

	void drawOpaque() const;
	void drawTranslucent() const;	
};
