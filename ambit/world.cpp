#include "rail.h"
#include "world.h"

void World::init()
{
	rail = new Rail();
	rail->init();
}

void World::tick(const float dt)
{
	rail->tick(dt);
}

void World::drawOpaque() const
{
	rail->drawOpaque();
}

void World::drawTranslucent() const
{
	rail->drawTranslucent();
}
