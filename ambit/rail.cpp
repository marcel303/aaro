#include "rail.h"

#include "framework.h"

/*

rail behaviors (choreography),

- take into consideration inertia and maximum force a of the moving object,
	- for realism, determine a desired location,
	- apply forces,
	- simulate taking weight (inertia) into account,
	- attempt to move into the desired location

- temperament,
	- integrate sensory data (weather, day-night, visitors.. ?)
	- decide on 'mood' (where mood could be: slow or fast, decisive or undecisive, agressive or calm, jittery or smooth.. ?)
 
- intent,
	- move to a specific location, and give some time to settle
	- large movements, small movements,
	- sit and wait, move fast (hunter),
	- sudden turn (opposition of thought, correction)

- tension,
	- silent vs lots of movement,
	- cool down period (fatigue),

rail extents,

- the rail has a min and a max extent. these define the range of values the mover is allowed to assume
- both are expressed in meters, and are considered 1d distances along a rail
- the exact shape of the rail is irrelevant to this simulation. it will just assume the rail is a straight one dimensional path. the path may be curved inside the physical world, but for now the simulation doesn't know, nor need to consider this


*/

const float min = -10.f;
const float max = +10.f;

void Rail::init()
{
	objectToWorld.MakeTranslation(10, 5, 0);
}

void Rail::tick(const float dt)
{
	position.Set(0, 0, 0);
}

void Rail::drawOpaque() const
{
	gxPushMatrix();
	{
		gxMultMatrixf(objectToWorld.m_v);

		setColor(colorWhite);
		fillCube(position, Vec3(.1f));
	}
	gxPopMatrix();
}

void Rail::drawTranslucent() const
{
	gxPushMatrix();
	{
		gxMultMatrixf(objectToWorld.m_v);
		
		setColor(255, 255, 255, 100);
		fillCube(position, Vec3(.12f));
	}
	gxPopMatrix();
}
