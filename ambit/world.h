#pragma once

struct Rail;

struct World
{
	Rail * rail = nullptr;

	void init();

	void tick(const float dt);

	void drawOpaque() const;
	void drawTranslucent() const;
};
