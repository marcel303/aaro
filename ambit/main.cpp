#include "world.h"

#include "magicavoxel-framework.h"

#include "forwardLighting.h"
#include "lightDrawer.h"
#include "renderer.h"
#include "renderOptions.h"
#include "shadowMapDrawer.h"

#include "framework.h"
#include "gx_mesh.h"

#include <algorithm>
#include <stdint.h>
#include <vector>

#define USE_OPTIMIZED_SHADOW_SHADER 1

using namespace rOne;

int main(int argc, char * argv[])
{
	setupPaths(CHIBI_RESOURCE_PATHS);
	
	framework.enableDepthBuffer = true;
	framework.enableRealTimeEditing = true;
	framework.filedrop = true;
	
	//framework.fullscreen = true;
	
	if (!framework.init(1200, 900))
		return 0;

	Renderer::registerShaderOutputs();
	
	World world;
	world.init();

	MagicaWorld mworld;
	readMagicaWorld("thespace.vox", mworld);

	Camera3d camera;
	camera.position.Set(0, 2, 0);
	//camera.mouseSmooth = .97f;
	//camera.maxForwardSpeed = 10.f;
	
	GxMesh drawMesh;
	GxVertexBuffer vb;
	GxIndexBuffer ib;
	
	gxCaptureMeshBegin(drawMesh, vb, ib);
	{
		drawMagicaWorld(mworld);
	}
	gxCaptureMeshEnd();
	
	RenderOptions renderOptions;
	renderOptions.renderMode = kRenderMode_ForwardShaded;
	//renderOptions.renderMode = kRenderMode_Flat;
	renderOptions.linearColorSpace = true;
	renderOptions.backgroundColor.Set(1, .5f, .25f);
	
	//renderOptions.screenSpaceAmbientOcclusion.enabled = true;
	renderOptions.screenSpaceAmbientOcclusion.strength = 1.f;
	
	//renderOptions.fog.enabled = true;
	renderOptions.fog.thickness = .001f;
	
	//renderOptions.debugRenderTargets = true;
	
	//renderOptions.chromaticAberration.enabled = true;
	renderOptions.chromaticAberration.strength = 1.f;
	
	renderOptions.bloom.enabled = true;
	renderOptions.bloom.blurSize = 20.f;
	renderOptions.bloom.strength = .2f;
	
	//renderOptions.lightScatter.enabled = true;
	renderOptions.lightScatter.strength = 1.f;
	renderOptions.lightScatter.numSamples = 100; // todo : bump to a higher number. fix light scatter getting more or less bright with # samples
	
	renderOptions.depthSilhouette.enabled = true;
	renderOptions.depthSilhouette.color.Set(0, 0, 0, .5f);
	
	renderOptions.fxaa.enabled = true;
	
	renderOptions.colorGrading.enabled = false;
	
	Renderer renderer;
	
	bool showSolids = false;
	bool showTranslucents = false;
	bool enableShadowMaps = false;
	
	ForwardLightingHelper helper;
	ShadowMapDrawer shadowMapDrawer;
	shadowMapDrawer.alloc(4, 1024);
	shadowMapDrawer.enableColorShadows = false;
	shadowMapDrawer.shadowMapFilter = kShadowMapFilter_PercentageCloser_3x3;
	
	GxTexture3d lookupTexture;
	
	for (;;)
	{
	#if !defined(DEBUG)
		mouse.showCursor(false);
		mouse.setRelative(true);
	#endif
	
		framework.process();

		if (framework.quitRequested)
			break;
		
		for (auto & file : framework.droppedFiles)
		{
			mworld.free();
			
			readMagicaWorld(file.c_str(), mworld);
			
			gxCaptureMeshBegin(drawMesh, vb, ib);
			{
				drawMagicaWorld(mworld);
			}
			gxCaptureMeshEnd();
		}
		
		if (keyboard.wentDown(SDLK_s))
			showSolids = !showSolids;
		
		if (keyboard.wentDown(SDLK_t))
			showTranslucents = !showTranslucents;
		
		if (keyboard.wentDown(SDLK_m))
			enableShadowMaps = !enableShadowMaps;
		
		camera.tick(framework.timeStep, true);
		
		renderOptions.colorGrading.lookupTextureFromSrgbColorTransform(
			[](Color & color)
			{
				color = color
					.hueShift(framework.time / 12.f)
					//.invertRGB(sinf(framework.time / 3.45f))
					.desaturate(sinf(framework.time / 2.34f));
			},
			lookupTexture);
		renderOptions.colorGrading.lookupTextureId = lookupTexture.id;
		
		framework.beginDraw(0, 0, 0, 0);
		{
			projectPerspective3d(90.f, .01f, 100.f);
			
			camera.pushViewMatrix();
			{
				// -- make light list --
				
				std::vector<Light> lights;
				
				if (true)
				{
					const Vec3 sunDirection = Vec3(.4f, -1.f, .4f).CalcNormalized();
					
					Light light;
					light.type = kLightType_Directional;
					light.position = camera.position - sunDirection * 50.f;
					light.direction = sunDirection;
					light.attenuationBegin = 0.f;
					light.attenuationEnd = 100.f;
					light.color.Set(1, 1, 1);
					light.intensity = .01f;
					
					lights.push_back(light);
				}
				
				if (true)
				{
					// camera following point light
					
					Light light;
					light.type = kLightType_Point;
					light.position = camera.position;
					light.attenuationBegin = 0.f;
					light.attenuationEnd = 4.f;
					light.color.Set(1, 1, 1);
					light.intensity = .1f;
					
					lights.push_back(light);
					
				// todo : add some kind of helper for building light lists
					//helper.addPointLight(camera.position, 0.f, 4.f, Vec3(1, 1, 1), .01f);
				}
				
				auto drawOpaqueBase = [&](const bool isMainPass)
				{
				#if USE_OPTIMIZED_SHADOW_SHADER
					Shader shader(
						isMainPass ?
							(
								renderOptions.renderMode == kRenderMode_ForwardShaded
								? "shader-forward"
								: "shader"
							)
						: "shader-shadow");
				#else
					Shader shader(
						renderOptions.renderMode == kRenderMode_ForwardShaded
						? "shader-forward"
						: "shader");
				#endif
					setShader(shader);
					
					int nextTextureUnit = 0;
					helper.setShaderData(shader, nextTextureUnit);
					shadowMapDrawer.setShaderData(shader, nextTextureUnit, camera.getViewMatrix());
					
					gxPushMatrix();
					{
						gxScalef(-1, 1, 1);
						gxRotatef(-90, 1, 0, 0);
						//gxScalef(.1f, .1f, .1f);
						
						drawMesh.draw();
					}
					gxPopMatrix();

					world.drawOpaque();
					
					if (showSolids)
					{
						beginCubeBatch();
						{
							for (int i = 0; i < 3*1000; ++i)
							{
								//const float s = .2f;
								const float s = lerp<float>(.04f, .14f, (cosf(framework.time + i) + 1.f) / 2.f);
								const float m = lerp<float>(-2.f, +2.f, (cosf(framework.time / 3.45f + i) + 1.f) / 2.f);
								Vec3 t;
								t[i % 3] = m;
								
								Assert(s >= 0.f);
								setColor(colorWhite);
								fillCube(
									Vec3(
										cosf(i / 1.23f) * 6.f,
										cosf(i / 2.34f) * 2.f,
										cosf(i * 1.23f) * 6.f) + t,
									Vec3(s, s, s));
							}
						}
						endCubeBatch();
					}
					
					clearShader();
				};
				
				auto drawOpaque = [&]()
				{
					drawOpaqueBase(true);
				};
				
				auto drawOpaqueShadow = [&]()
				{
					drawOpaqueBase(false);
				};
				
				auto drawTranslucent = [&]()
				{
					// todo : shader setup
					//world.drawTranslucent();
				};
				
				// -- prepare shadow maps --
				
				size_t lightId = 0;
				
				if (enableShadowMaps)
				{
					for (auto & light : lights)
					{
						if (light.type == kLightType_Directional)
						{
							Mat4x4 lightToWorld;
							lightToWorld.MakeLookat(light.position, light.position + light.direction, Vec3(0, 1, 0));
							lightToWorld = lightToWorld.CalcInv();
						
							shadowMapDrawer.addDirectionalLight(
								lightId,
								lightToWorld,
								light.attenuationBegin,
								light.attenuationEnd,
								8.f);
						}
						else if (light.type == kLightType_Spot)
						{
							Mat4x4 lightToWorld;
							lightToWorld.MakeLookat(light.position, light.position + light.direction, Vec3(0, 1, 0));
							lightToWorld = lightToWorld.CalcInv();
						
							shadowMapDrawer.addSpotLight(
								lightId,
								lightToWorld,
								light.spotAngle,
								light.attenuationBegin,
								light.attenuationEnd);
						}
						
						lightId++;
					}
				}
				
				helper.prepareShaderData(4, 64.f, false, camera.getViewMatrix());
				
				shadowMapDrawer.drawOpaque = drawOpaqueShadow;
				shadowMapDrawer.drawTranslucent = drawTranslucent;
				shadowMapDrawer.drawShadowMaps(camera.getViewMatrix());
				
				helper.reset();
				
				// -- prepare lighting data --
				
				lightId = 0;
				
				for (auto & light : lights)
				{
					if (light.type == kLightType_Directional)
					{
						helper.addDirectionalLight(
							light.direction,
							light.color,
							light.intensity,
							shadowMapDrawer.getShadowMapId(lightId));
					}
					else if (light.type == kLightType_Point)
					{
						helper.addPointLight(
							light.position,
							light.attenuationBegin,
							light.attenuationEnd,
							light.color,
							light.intensity,
							shadowMapDrawer.getShadowMapId(lightId));
					}
					else if (light.type == kLightType_Spot)
					{
						helper.addSpotLight(
							light.position,
							light.direction,
							light.spotAngle,
							light.attenuationEnd,
							light.color,
							light.intensity,
							shadowMapDrawer.getShadowMapId(lightId));
					}
				
					lightId++;
				}
				
				helper.prepareShaderData(32, 64.f, true, camera.getViewMatrix());
				
				// -- render --
				
				RenderFunctions renderFunctions;
				renderFunctions.drawOpaque = drawOpaque;
				renderFunctions.drawTranslucent = drawTranslucent;
				
				renderer.render(renderFunctions, renderOptions, framework.timeStep);
			}
			camera.popViewMatrix();
			
			projectScreen2d();
			
			renderOptions.debugRenderTargets = keyboard.isDown(SDLK_1);
				
			if (keyboard.isDown(SDLK_2))
				shadowMapDrawer.showRenderTargets();
		}
		framework.endDraw();
		
		helper.reset();
		shadowMapDrawer.reset();
	}
	
	// todo : shutdown renderer
	
	framework.shutdown();

	return 0;
}
