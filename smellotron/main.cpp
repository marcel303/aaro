#include "Ease.h"
#include "framework.h"
#include "oscReceiver.h"
#include "StringEx.h"

// serial includes
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

// the number of channels that can be automated and for which analog values are sent to the Arduino
static const int kNumChannels = 8;

// for linking channel values to the first channel. default = off
static bool s_linkEnable[kNumChannels] = { };

// set to false when Reaper tells us to stop
static bool s_isPlaying = true;

// timer and animation to enter energy consevative mode after five minutes of mouse/keyboard inactivity
static const int kInactivityTime = 60;
static float s_inactivityTimer = kInactivityTime;
static float s_inactivityAnimation = 1.f;

struct FadeTimer
{
	FadeTimer(const float x, const float y)
	{
		startTime = x;
		time = y;
		timeRcp = 1.f / y;
	}
	
	float startTime;
	float time;
	float timeRcp;
	
	float get() const
	{
		return 1.f - time * timeRcp;
	}
};

static float s_fadeTime = 0.f;

static FadeTimer s_zoomFade = { 2.f, 2.f };
static FadeTimer s_lightFade = { 2.f, 2.f };
static FadeTimer s_uiFade = { 4.f, 2.f };

static FadeTimer * s_fades[] =
{
	&s_zoomFade,
	&s_lightFade,
	&s_uiFade
};

enum Message
{
	kMessage_None = 0,
	kMessage_SetValue = 1,   // message sent to the Arduino to set the channel value (0..100)
	kMessage_ReportValue = 2 // message received from the Arduino reporting its channel value (0..100)
};

struct SerialIo
{
	int port = -1; // serial port handle
	
	bool init(const char * path, const int baudRate)
	{
		Assert(port == -1);
		
		// attempt to open the given serial port
		
		port = open(path, O_RDWR | O_NOCTTY | O_NDELAY);
		
		if (port < 0)
		{
			logError("failed to open serial port: %s", path);
		}
		else
		{
			// attempt to set baud rate and some other stuff needed for interfacing with the Arduino
			
			struct termios options;

			// Open the device in nonblocking mode
			fcntl(port, F_SETFL, FNDELAY);

			// Set parameters
			tcgetattr(port, &options); // Get the current options of the port
			bzero(&options, sizeof(options)); // Clear all the options
			
			// Set the baud rate
			speed_t speed;
			switch (baudRate)
			{
				case    110: speed = B110;    break;
				case    300: speed = B300;    break;
				case    600: speed = B600;    break;
				case   1200: speed = B1200;   break;
				case   2400: speed = B2400;   break;
				case   4800: speed = B4800;   break;
				case   9600: speed = B9600;   break;
				case  19200: speed = B19200;  break;
				case  38400: speed = B38400;  break;
				case  57600: speed = B57600;  break;
				case 115200: speed = B115200; break;
				default:
					return false;
			}
			
			cfsetispeed(&options, speed);
			cfsetospeed(&options, speed);
			
			// Configure the device : 8 bits, no parity, no control
			options.c_cflag |= CLOCAL | CREAD | CS8;
			options.c_iflag |= IGNPAR | IGNBRK;
			
			options.c_cc[VTIME] = 0; // Timer unused
			options.c_cc[VMIN] = 0; // At least on character before satisfy reading
			
			// Activate the settings
			tcsetattr(port, TCSANOW, &options);
		}
		
		return isConnected();
	}
	
	void shut()
	{
		if (port >= 0)
		{
			// close the serial port
		
			close(port);
			port = -1;
		}
	}

	bool isConnected() const
	{
		return port >= 0;
	}

	bool send(const void * bytes, const int numBytes) const
	{
		if (port < 0)
		{
			return false;
		}
		else
		{
			// send the bytes over the serial connection
			
			const int r = write(port, bytes, numBytes);
			
			if (r != numBytes)
			{
				logError("write returned error code %d", r);
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	
	bool receive(uint8_t & c) const
	{
		if (read(port, &c, 1) == 1)
			return true;
		else
			return false;
	}
};

struct SerialDetection
{
	std::vector<std::string> ports;
	
	void detect()
	{
		// clear previous detection result (if any)
		
		ports.clear();
		
		// generate a list of all devices
		
		std::vector<std::string> files = listFiles("/dev", false);
		
		for (auto & file : files)
		{
			// filter devices by 'tty.usbmodem', which is the name used by Arduino tty devices
			
			if (String::StartsWith(file, "/dev/tty.usbmodem") || // Arduino Uno (original)
				String::StartsWith(file, "/dev/tty.usbserial") ||
				String::StartsWith(file, "/dev/tty.wchusbserial"))
			{
				logDebug("found serial: %s", file.c_str());
				
				ports.push_back(file);
			}
		}
	}
};

struct ArduinoIo
{
	// ReceiveState is used to receive multi-byte messages from the Arduino
	struct ReceiveState
	{
		uint8_t state = 0xff;
		
		Message message = kMessage_None;
		uint8_t value1 = 0;
		uint8_t value2 = 0;
	};
	
	SerialIo serialIo;
	
	ReceiveState receiveState;
	
	bool receiveDone = false; // true when a multi-byte message has been received. the message is stored in receiveState
	
	bool init(const char * path, const int baudRate)
	{
		return serialIo.init(path, baudRate);
	}
	
	void shut()
	{
		serialIo.shut();
	}

	bool isConnected() const
	{
		return serialIo.isConnected();
	}

	bool send(const uint8_t target, const uint8_t message, const uint8_t value) const
	{
		if (isConnected() == false)
		{
			return false;
		}
		else
		{
			// send the four byte message over the serial connection
			
			const uint8_t bytes[4] = { 0xff, target, message, value };
			
			return serialIo.send(bytes, 4);
		}
	}
	
	bool receive()
	{
		receiveDone = false;
		
		uint8_t c = 0;
		
		if (serialIo.receive(c) == false)
		{
			return false;
		}
		else
		{
			// is this the start byte? if so, begin receiving a new multi-byte message
			
			if (c == 0xff)
				receiveState.state = 0;
			
			// are we in the middle of receiving a multi-byte message?
			
			if (receiveState.state != 0xff)
			{
				// if so, increment the state counter, store the current byte, and check if we're done
				
				receiveState.state++;
				
				if (receiveState.state == 2)
					receiveState.message = (Message)c;
				if (receiveState.state == 3)
					receiveState.value1 = c;
				if (receiveState.state == 4)
					receiveState.value2 = c;
				
				if (receiveState.state == 4)
				{
					// we're done receiving a multi-byte message. reset the state to idle and set the receiveDone flag
					
					receiveState.state = 0xff;
					
					receiveDone = true;
				}
			}
			else
			{
				// just show the received characters if we're not in the middle of receiving a multi-byte message
				// this is useful for when the Arduino wants to report descriptive error or warning messages back to us
				
				printf("%c", (char)c);
			}
			
			return true;
		}
	}
};

struct ChannelObject
{
	struct Properties
	{
		int index = 0;
	};
	
	Properties properties;
	
	float desiredValue = 0.f;
	
	float receivedValue = 0.f;
	
	float value = 0.f;

	bool init()
	{
		return true;
	}
	
	void tick(const float dt)
	{
		value =
			keyboard.isDown(SDLK_a)
			? (s_isPlaying ? desiredValue : 0)
			: receivedValue;
	}
	
	void drawBulb() const
	{
		const float t = (properties.index + .5f) / kNumChannels * 2 - 1;
		const float scale = s_inactivityAnimation;
		
		gxPushMatrix();
		{
			gxTranslatef(0, -.5f, 0);
			gxRotatef(-90, 1, 0, 0);
			
			Model model("bulb.fbx");
			Shader shader("shaders/bulb");
			setShader(shader);
			if (s_linkEnable[properties.index])
				shader.setImmediate("lightAmount", value * s_lightFade.get() + sinf(framework.time * 10.f) * .03f);
			else
				shader.setImmediate("lightAmount", value * s_lightFade.get());
			model.overrideShader = &shader;
			model.drawEx(
				Vec3(t * 6, 0, 0),
				Vec3(0, 1, 0), 0.f,
				10.f * scale,
				DrawMesh | DrawUnSkinned);
		}
		gxPopMatrix();
	}
	
	void drawOpaque() const
	{
		drawBulb();
	}
	
	void drawTranslucent() const
	{
	}
	
	void drawScreenOverlay() const
	{
		int viewSx, viewSy;
		framework.getCurrentViewportSize(viewSx, viewSy);
		
		const float t = (properties.index + .5f) / kNumChannels * 2 - 1;
		const float s = lerp(.9f, .1f, desiredValue);
		
		const int sx = 20;
		const int sy = 30;
		
		hqBegin(HQ_FILLED_ROUNDED_RECTS);
		setLumi(s_linkEnable[properties.index] ? 140 + sinf(framework.time * 10.f) * 30 : 100);
		setAlphaf(s_uiFade.get());
		hqFillRoundedRect(
			viewSx/2 + t * 100 - sx/2,
			viewSy * 4/5 + sy * s - 1,
			viewSx/2 + t * 100 + sx/2,
			viewSy * 4/5 + sy * s + 1, 4);
		hqEnd();
		
		hqBegin(HQ_STROKED_ROUNDED_RECTS);
		setLumi(10);
		setAlphaf(s_uiFade.get());
		hqStrokeRoundedRect(
			viewSx/2 + t * 100 - sx/2,
			viewSy * 4/5,
			viewSx/2 + t * 100 + sx/2,
			viewSy * 4/5 + 30, 4, 2);
		hqEnd();
	}
};

struct World
{
	ChannelObject channelObjects[kNumChannels];
	
	bool init()
	{
		bool result = true;
		
		int index = 0;
		
		for (auto & channelObject : channelObjects)
		{
			channelObject.properties.index = index++;
			
			result &= channelObject.init();
		}
		
		return result;
	}
	
	void shut()
	{
	
	}
	
	void tick(const float dt)
	{
		// mirror channel value of the first channel when linked
		
		for (int i = 1; i < kNumChannels; ++i)
			if (s_linkEnable[i])
				channelObjects[i].desiredValue = channelObjects[0].desiredValue;
		
		for (auto & channelObject : channelObjects)
			channelObject.tick(dt);
	}
};

struct TimelineDataReceiver : OscReceiveHandler
{
	World * world = nullptr;
	
	void init(World * in_world)
	{
		world = in_world;
	}
	
	virtual void handleOscMessage(const osc::ReceivedMessage & m, const IpEndpointName & remoteEndpoint)
	{
		bool handled = false;
		
		for (int i = 0; i < kNumChannels; ++i)
		{
			auto & channelObject = world->channelObjects[i];
			
			char address[64];
			sprintf_s(address, sizeof(address), "/track/%d/analog-value", channelObject.properties.index + 1);
			
			if (strcmp(m.AddressPattern(), address) == 0)
			{
				handled = true;
				
				for (auto a_itr = m.ArgumentsBegin(); a_itr != m.ArgumentsEnd(); ++a_itr)
				{
					auto & a = *a_itr;
					
					if (a.IsFloat())
						channelObject.desiredValue = a.AsFloatUnchecked();
					else if (a.IsDouble())
						channelObject.desiredValue = a.AsDoubleUnchecked();
				}
			}
		}
		
		// handle /play 0|1 coming from Reaper
		
		if (strcmp(m.AddressPattern(), "/play") == 0)
		{
			for (auto a_itr = m.ArgumentsBegin(); a_itr != m.ArgumentsEnd(); ++a_itr)
			{
				auto & a = *a_itr;
				
				if (a.IsBool())
					s_isPlaying = a.AsBoolUnchecked();
				else if (a.IsInt32())
					s_isPlaying = a.AsInt32Unchecked();
				else if (a.IsFloat())
					s_isPlaying = a.AsFloatUnchecked();
			}
		}
		
		if (handled == false)
		{
			//logDebug("received unknown OSC message: %s", m.AddressPattern());
		}
	}
};

int main(int argc, char * argv[])
{
	setupPaths(CHIBI_RESOURCE_PATHS);
	
	framework.enableDepthBuffer = true;
	//framework.allowHighDpi = false;
	framework.msaaLevel = 4;
	
#if defined(DEBUG)
	framework.enableRealTimeEditing = true;
#endif

	if (!framework.init(800, 300))
		return -1;
	
	ArduinoIo arduinoIo;
	
	{
		SerialDetection serialDetection;
		serialDetection.detect();
		
		if (serialDetection.ports.empty())
			logWarning("serial detection didn't find any candidate ports");
		else if (arduinoIo.init(serialDetection.ports[0].c_str(), 9600) == false)
			logError("failed to open serial port");
	}

	OscReceiver oscReceiver;
	if (!oscReceiver.init("255.255.255.255", 4002))
		logError("failed to initialize OSC receiver");

	World world;
	if (world.init() == false)
		logError("failed to initialize world");
	
	TimelineDataReceiver timelineDataReceiver;
	timelineDataReceiver.init(&world);

	float sendTimer = 0.f; // timer for sending data to the Arduino at a fixed interval
	float reconnectTimer = 6.f; // timer for the auto-reconnect feature
	float disconnectedBlinkTimer = 0.f; // timer for the disconnected blink animation
	
	for (;;)
	{
		// process events
		
		framework.process();

		// process input
		
		if (framework.quitRequested)
			break;

		// conserve energy when the user left the app inactive for a while
		
		bool wasDelayed = false;
		
		if (s_inactivityTimer == 0.f && s_inactivityAnimation == 0.f)
		{
			SDL_Delay(100);
			wasDelayed = true;
		}

		// inactivity detection
		
		if (keyboard.isIdle() && mouse.isIdle())
			s_inactivityTimer = fmaxf(0.f, s_inactivityTimer - framework.timeStep);
		else
			s_inactivityTimer = kInactivityTime;
		
		// inactivity animation
		
		if (wasDelayed == false)
		{
			if (s_inactivityTimer == 0.f)
				s_inactivityAnimation = fmaxf(0.f, s_inactivityAnimation - framework.timeStep / .4f);
			else
				s_inactivityAnimation = fminf(1.f, s_inactivityAnimation + framework.timeStep / .4f);
		}
		
		// arduino connection
		
		if (arduinoIo.isConnected())
		{
			reconnectTimer = 6.f;
		}
		else
		{
			// decrement the reconnect timer when we're disconnected from the Arduino
			// when the timer hits zero, we'll attemp a reconnect (see below)
			
			reconnectTimer = fmaxf(0.f, reconnectTimer - framework.timeStep);
			
			// when disconnected, also reset the received value for the channels
			// this provides a visual cue the connection is broken
			for (auto & channelObject : world.channelObjects)
				channelObject.receivedValue = 0.f;
		}
		
		if (keyboard.wentDown(SDLK_SPACE) || (reconnectTimer == 0.f))
		{
			reconnectTimer = 6.f;
			
			// first disconnect existing serial connection (if any)
			
			arduinoIo.shut();
			
			// attempt to reconnect by first searching for serial ports, and then connecting to the first candidate found
			
			SerialDetection serialDetection;
			serialDetection.detect();
			
			if (serialDetection.ports.empty())
				logWarning("serial detection didn't find any candidate ports");
			else if (arduinoIo.init(serialDetection.ports[0].c_str(), 9600) == false)
				logError("failed to open serial port");
			
			// start the blink animation when the connection attempt failed
			
			if (arduinoIo.isConnected() == false)
				disconnectedBlinkTimer = 1.f;
		}
		
		// toggle link option when key 2..N is pressed
		
		for (int i = 1; i < kNumChannels; ++i)
			if (keyboard.wentDown(SDLK_1 + i))
				s_linkEnable[i] = !s_linkEnable[i];
		
		// receive timeline data over OSC
		
		oscReceiver.flushMessages(&timelineDataReceiver);
		
		// tick world
		
		const float dt = framework.timeStep;

		world.tick(dt);
		
		// tick animations
		
		for (auto * fade : s_fades)
			if (s_fadeTime >= fade->startTime)
				fade->time = fmaxf(0.f, fade->time - dt);
		
		s_fadeTime += dt;
		
		// update the disconnected blink animation
		
		disconnectedBlinkTimer = fmaxf(0.f, disconnectedBlinkTimer - framework.timeStep / .5f);
		
		// send output
		
		sendTimer = fmaxf(0.f, sendTimer - dt);
		
		if (sendTimer == 0.f)
		{
			sendTimer = .1f;
			
			for (int i = 0; i < kNumChannels; ++i)
			{
				auto & channelObject = world.channelObjects[i];
				
				// is playback active? if so, send the value we received over OSC. otherwise send zero
				
				const int value =
					s_isPlaying
					? (int)roundf(channelObject.desiredValue * 100.f)
					: 0;
				
				if (arduinoIo.send(i, kMessage_SetValue, value) == false)
					arduinoIo.shut();
			}
		}
		
		// receive messages from the Arduino
		
		while (arduinoIo.receive())
		{
			if (arduinoIo.receiveDone)
			{
				//logDebug("receive done");
				
				if (arduinoIo.receiveState.message == kMessage_ReportValue)
				{
					const int index = arduinoIo.receiveState.value1;
					const int value = arduinoIo.receiveState.value2;
					
					logDebug("value: %d: %d", index, value);
					
					if (index >= 0 && index < kNumChannels)
					{
						auto & channelObject = world.channelObjects[index];
						
						channelObject.receivedValue = value / 100.f;
					}
				}
				else
				{
					logWarning("received unknown message");
				}
			}
		}

		// draw
		
		framework.beginDraw(40, 40, 40, 0);
		{
			setColor(colorWhite);
			setFont("calibri.ttf");
			
			int y = 6;
			
			if (arduinoIo.isConnected())
			{
			/*
				setAlphaf(s_lightAmount);
				drawText(6, y, 14, +1, +1, "(Connected) Press SPACE to (re)connect");
				y += 20;
			*/
			}
			else
			{
				setAlphaf(s_uiFade.get() * .24f + disconnectedBlinkTimer);
				drawText(6, y, 14, +1, +1, "(Disconnected) Press SPACE to (re)connect. Auto-reconnect in T-%d", (int)ceilf(reconnectTimer));
				y += 20;
			}
			
			setAlphaf(1.f);
			
		#if 0
			for (auto & channelObject : world.channelObjects)
			{
				drawText(4, y, 12, +1, +1, "desired: %d, current: %d",
					(int)roundf(channelObject.desiredValue * 100.f),
					(int)roundf(channelObject.receivedValue * 100.f));
				
				y += 18;
			}
		#endif
			
			projectPerspective3d(45.f * EvalEase(s_zoomFade.get(), kEaseType_SineOut, 1.f), .01f, 10.f);
			Camera3d camera;
			camera.position.Set(0, 0, -6);
			camera.pushViewMatrix();
			{
				pushDepthTest(true, DEPTH_LESS);
				pushBlend(BLEND_OPAQUE);
				{
					if (s_inactivityAnimation > 0.f)
					{
						for (auto & channelObject : world.channelObjects)
						{
							channelObject.drawOpaque();
						}
					}
				}
				popBlend();
				popDepthTest();
				
				pushDepthTest(true, DEPTH_LESS, false);
				pushBlend(BLEND_ADD);
				{
					if (s_inactivityAnimation > 0.f)
					{
						for (auto & channelObject : world.channelObjects)
						{
							channelObject.drawTranslucent();
						}
					}
				}
				popBlend();
				popDepthTest();
			}
			camera.popViewMatrix();
			
			projectScreen2d();
			
			for (auto & channelObject : world.channelObjects)
			{
				channelObject.drawScreenOverlay();
			}
			
			const float overlayFade = 1.f - s_lightFade.get();
			
			if (overlayFade > 0.f)
			{
				setAlphaf(overlayFade);
				setLumif(.6f);
				drawText(800/2, 300/2, 48, 0, 0, "SMelloTron");
			}
		}
		framework.endDraw();
	}
	
	world.shut();
	
	oscReceiver.shut();
	
	arduinoIo.shut();

	framework.shutdown();

	return 0;
}
