/*

SMelloTron Arduino script
Marcel Smit (c) 2020

Drive N digital output channels,
Using N analog input values,
Received through the serial interface of the Arduino

Hooks up with the SMelloTron desktop app,
which allows it to be used in conjunction with
the Tosca VST plugin developed by IRCAM

Provided is also a tosca mapping file,
exposing the parameters and their names,
which allows any DAW to send OSC to the
SMelloTron desktop app

*/

#ifndef LED_BUILTIN
#define LED_BUILTIN BUILTIN_LED
#endif

static const int kNumChannels = 8;

static const int channel_pins[kNumChannels] =
{
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11
};

enum Message
{
  kMessage_None = 0,
  kMessage_SetValue = 1,
  kMessage_ReportValue = 2
};

struct ChannelData
{
  int pin = -1;

  unsigned long lastTick = 0;
  
  int8_t currentValue = 0;
  int8_t desiredValue = 0;
};

static ChannelData channelData[kNumChannels];

static uint8_t read_state = 0xff;
static uint8_t read_value1 = 0;
static uint8_t read_value2 = 0;
static uint8_t read_value3 = 0;

static unsigned long lastLoopTick = 0;

static int32_t blinkPhase = 0;

static int32_t receiveTimeoutTimer = 0;

void setup()
{
  Serial.begin(9600);
  
  pinMode(LED_BUILTIN, OUTPUT);

  for (int i = 0; i < kNumChannels; ++i)
  {
    channelData[i].pin = channel_pins[i];
    pinMode(channel_pins[i], OUTPUT);
    digitalWrite(channel_pins[i], HIGH);
  }
}

void loop()
{
  //delay(10);
  
  while (Serial.available() > 0)
  {
    const uint8_t value = Serial.read();

    //Serial.print("rcv ");
    //Serial.println(value);

    receiveTimeoutTimer = 0;
    
    if (value == 0xff)
      read_state = 0;

    if (read_state != 0xff)
    {
      read_state++;
  
      if (read_state == 2)
        read_value1 = value;
      if (read_state == 3)
        read_value2 = value;
      if (read_state == 4)
        read_value3 = value;
    }

    if (read_state == 4)
    {
      read_state = 0xff;
      
      const uint8_t target = read_value1;
      const uint8_t message = read_value2;
      const uint8_t value = read_value3;

      if (target >= 0 && target < kNumChannels)
      {
        ChannelData & c = channelData[target];

        if (message == kMessage_SetValue)
        {
          if (value >= 0 && value <= 100)
            c.desiredValue = value;
          else
            Serial.println("value out of range");  
        }
      }
      else
      {
        Serial.println("target out of range");  
      }
    }
  }

  const unsigned long tick = millis();
  
  for (ChannelData & c : channelData)
  {
    if (receiveTimeoutTimer > 2000)
    {
      c.desiredValue = 0;
    }
    
    if (tick < c.lastTick)
    {
      // millis wraps around every 50 days
      // reset lastTick when this happens
      
      Serial.println("resetting lastTick due to millis wrap around");
      
      c.lastTick = 0;
    }
    else
    {
      c.lastTick = tick;
      
      c.currentValue = c.desiredValue >= 50 ? 100 : 0;

      // update the digital on/off output for this channel

      digitalWrite(c.pin, c.currentValue == 0 ? HIGH : LOW);
    }
  }

  if (lastLoopTick == 0)
    lastLoopTick = tick;

  if (tick > lastLoopTick)
  {
    const int32_t elapsedMillis = tick - lastLoopTick;

    receiveTimeoutTimer += elapsedMillis;

    // blink the built-in led at a rate proportional to the value of the first channel

    if (elapsedMillis > 0 && elapsedMillis < 256)
    {
      blinkPhase += 256 * elapsedMillis * channelData[0].currentValue * 6 / 100;

      const int32_t phaseWrap = int32_t(256) * 1000;
      while (blinkPhase > phaseWrap)
        blinkPhase = blinkPhase - phaseWrap;
    }

    if (blinkPhase < 0)
    {
      Serial.println("resetting blinkPhase due to it being less than 0..");
      blinkPhase = 0;
    }
    
    digitalWrite(LED_BUILTIN, blinkPhase < int32_t(128) * 1000 ? LOW : HIGH);

#if 0
    Serial.print("phase ");
    Serial.print(blinkPhase);
    Serial.print(" value ");
    Serial.print(channelData[0].currentValue);
    Serial.print(" millis ");
    Serial.print(elapsedMillis);
    Serial.println();
#endif

    // report back the current values used to drive the outputs

    for (int i = 0; i < kNumChannels; ++i)
    {
      const uint8_t bytes[4] =
      {
        0xff,
        kMessage_ReportValue,
        i,
        channelData[i].currentValue
      };

      Serial.write(bytes, 4);
    }
  }

  lastLoopTick = tick;
}

